<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;


class createController extends Controller
{
    public function create()
    {
    	return view('create');
    }
    public function index()
    {
        $product=DB::table('product')->paginate(8);
        return view('welcome',['product'=>$product]);
    }
    public function store(Request $request)
    {
    	//print_r($request->all());
    		$this->validate($request,
			[
				'pname'=>'required',
				'pdetail'=>'required',
			]
		);
    	$product=new Product;
		$product->name=$request->pname;
		$product->description=$request->pdetail;
        $product->trailer=$request->trailer;
        if($request->hasfile('image'))
        {
            $file=$request->file('image');
            $extension=$file->getClientOriginalExtension();
            $filename=time().'.'.$extension;
            $file->move('public/upload/userimg/',$filename);
            $product->image=$filename;
        }
        else
        {
            return $request;
            $product->image='';
        }
		$product->save();
		return redirect(route('home'))->with('succmsg','Movie added successfully');
    }
    public function view($id)
    {
    	$all=Product::find($id);
    	return view('view',compact('all'));
        // $product=DB::table('product')->paginate(4);
        // return view('view',['product'=>$product]);
    }
    public function edit($id)
    {
    	$all=Product::find($id);
    	return view('edit',compact('all'));
    }
      public function update(Request $request,$id)
    {
    	//print_r($request->all());
    		$this->validate($request,
			[
				'pname'=>'required',
				'pdetail'=>'required',
			]
		);
    	$product=Product::find($id);
		$product->name=$request->pname;
		$product->description=$request->pdetail;
        $product->trailer=$request->trailer;
		$product->save();
		return redirect(route('home'))->with('updatemsg','Movie Updated successfully');
    }
    public function delete($id)
    {
    	Product::find($id)->delete();
    	return redirect(route('home'));
    }
    // public function invoice()
    // {
    //     $pdf = PDF::loadView('invoice');
    //     return $pdf->download('invoice.pdf');
    // }
}
