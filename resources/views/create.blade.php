<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <style>
         body
        {
          background-image: url('resources/views/layout/bg.jpg');
          height: 95vh;
          width: auto;
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
        }
        label
        {
          color: #fff;
        }
      </style>
    </head>

    <body>
       
@extends('layout.main')
@section('content')

<div class="row">
	<div class="col-12">
		@if($errors->any())
		<h3>Hey , You got some issues</h3>
		<ul class="list-group">
			@foreach($errors->all() as $error)
				<li class="list-group-item list-group-item-danger">{{ $error }}</li>	
			@endforeach
		</ul>
		@endif
	</div>
</div>



    <div class="row mt-5">
    	<div class="col-2"></div>
    	<div class="col-8">
    		<form method="post" action="{{ route('store') }}" enctype="multipart/form-data">
    			{{ csrf_field() }}
  <div class="form-group">
    <label for="exampleFormControlInput1">Movie Name</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter the product name" name="pname">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Movie Storyline</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="pdetail"></textarea>
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Movie Trailer Link</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Trailer Link" name="trailer">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Movie Poster</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image">
  </div>
  <div class="row">
  	<div class="col-12 text-center">
  		<button class="btn btn-primary btn-lg">Add Movie</button>
  	</div>
  </div>
</form>
    	</div>
    	<div class="col-2"></div>
    </div>

@endsection
    




    </body>
</html>
