<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <style>
        img
        {
          border-radius: 20px;
          filter: drop-shadow(0 0 0.75rem #7C95CE);
        }
        .comment
        {
          height: 300px;
          max-height: 300px;
          overflow-y: scroll;
          border:1px solid #000;
          background-color: #7A549B;
        }
        .comment li
        {
          border:none;
          text-align: right;
        }
        .comment1 li
        {
            border-radius: 10px;
        }
        h2
        {
          color: #fff;
        }
         body
        {
          background-image: url('../resources/views/layout/bg.jpg');
    height: 200vh;
    width: auto;
     background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
        }
        message
        {
          background-color: green;s
        }
      </style>
    </head>
    <body>
       
@extends('layout.main')
@section('content')

    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="row">
  <div class="col-12 text-center p-5">
    <h2>{{ $all->name }}</h2>
  </div>

</div>
<div class="row">
  <div class="col-6">
     <img src="{{asset('public/upload/userimg/'.$all->image)}}" height="550px" width="400px">
  </div>
  <div class="col-6">
    <div class="row p-3 text-center">
      <div class="col-12">
    <h2>Movie Storyline</h2>
 </div>
</div>
    <div class="row">
      <div class="col-12">
   <div class="card text-white bg-primary mb-3">
      <div class="card-body">
        <p class="card-text"> {{ $all->description }} </p>
      </div>
   </div>
 </div>
</div>
 <div class="row text-center">
      <div class="col-12">
    <div class="card" style="height: 180%;border:none; background: transparent;">
  {{-- <iframe src=" {{ $all->trailer}}" style="height: 180%;border-radius: 10px;border:none;"> --}}
    <iframe width="560" height="180%" src="{{ $all->trailer}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</iframe>
</div>
 </div>
</div>

  </div>

</div>
<div class="row">
  <div class="col-6">
    
  </div>
  <div class="col-6">
    <div class="row my-3">
      <div class="col-6">
        <button class="btn btn-success container-fluid">Download</button>
      </div>
      <div class="col-6">
        <button class="btn btn-warning container-fluid">Watch Online</button>
      </div>
    </div>
  </div>

</div>
        </div>
        <div class="col-2"></div>
    </div>



    {{-- comment section --}}
<div id="app">
<div class="row my-3">
  <div class="col-2"></div>
  <div class="col-8">
    <h2>Comments </h2>
  </div>
  <div class="col-2"></div>
</div>
<div class="row my-3">
  <div class="col-2"></div>
  <div class="col-8">
    <ul class="list-group comment" v-chat-scroll>
  <message  v-for="item in allmsg" :key="item.id" color='warning'>@{{ item }}</message>
  
</ul>


  </div>
  <div class="col-2"></div>
</div>
<div class="row">
  <div class="col-2"></div>
  <div class="col-8">
  <input type="text" name="comment" class="form-control my-2" v-model='message' v-on:keyup.enter='send'>
<button class="btn btn-primary my-2" v-on:click="send">send</button></div>
  <div class="col-2"></div>
</div>
</div>
<script src="{{asset('public/js/app.js')}}"></script>
@endsection
    




    </body>
</html>
