<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>
      <style>
        img:hover
        {
          filter: drop-shadow(0 0 0.75rem #7C95CE);
          transition: 0.5s all;
        }
        img
        {
filter: brightness(120%);
        }
        body
        {
          background-image: url('resources/views/layout/bg.jpg');
    height: 160vh;
    width: auto;
     background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
        }
        .card
        {
          background: transparent!important;
          color: #fff;
          font-weight: bold;
          
        }
      </style>
    </head>
    <body>
       
@extends('layout.main')
@section('content')

@if(session('succmsg'))
{{-- <div class="alert alert-success alert-dismissible fade show text-center my-3" role="alert">
  <strong>Movie Addedd Successfully</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div> --}}
<script>

// swal("Added", "Movie Added successfully!!", "success");
swal("Movie Added Successfully -:)");
</script>
@else
  @if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
    {{$error}}
    </div>
    @endforeach
  @endif
@endif

@if(session('updatemsg'))

  <script>

// swal("Added", "Movie Added successfully!!", "success");
    swal("Movie updated Successfully -:)");
</script>

@endif

{{-- <div class="row">
  <div class="col-12">
        <a class="nav-link btn btn-primary" href="{{ url('/invoice') }}">Download Movie List <span class="sr-only">(current)</span></a>

  </div>
</div> --}}
    <div class="row mt-5">
{{--         <div class="col-2"></div>
 --}}        <div class="col-12">
  
    <div class="row">
      <div class="col-1"></div>
      <div class="col-10">
        <div class="row">
        @foreach($product as $data)
        <div class="col-3">
        <div class="card p-2" style="width: 18rem;border:none;">
 <a href="{{route('view',$data->id)}}"><img src="{{asset('public/upload/userimg/'.$data->image)}}" style="height: 350px;border-radius: 10px;" class="card-img-top" alt="..."></a>
  <div class="card-body">
    <p class="card-text text-center">{{ $data->name }}</p>
    <div class="row">
      <div class="col-6 text-center">
          <a href="{{route('edit',$data->id)}}"> <button class="btn btn-warning">Edit &nbsp<i class="fa fa-pencil" aria-hidden="true"></i>
</button></a>
      </div>
      <div class="col-6 text-center">
        <a href="{{route('delete',$data->id)}}"><button class="btn btn-danger">Delete  &nbsp<i class="fa fa-trash" aria-hidden="true"></i>
</button></a>
      </div>
    </div>
  </div>
</div></div>
        @endforeach
        
      </div>
    </div>
      <div class="col-1">
      </div>

        </div>
        <div class="row">
          <div class="col-4"></div>
          <div class="col-4 text-center">
             {{ $product->links() }}

          </div>
          <div class="col-4"></div>
        </div>
{{--         <div class="col-2"></div>
 --}}    </div>



@endsection
    


{{-- @include('sweetalert::alert') --}}

    </body>
</html>
