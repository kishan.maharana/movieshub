<?php

use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	 Alert::success('Success Title', 'Success Message');
    return view('welcome');
});
Route::get('/create','createController@create')->name('create');
Route::get('/','createController@index')->name('home');
Route::post('/store','createController@store')->name('store');
Route::get('/view/{id}','createController@view')->name('view');
Route::get('/edit/{id}','createController@edit')->name('edit');
Route::post('/update/{id}','createController@update')->name('update');


Route::get('/invoice',function(){
	$pdf = PDF::loadView('invoice');
return $pdf->download('invoice.pdf');
});


Route::get('/delete/{id}','createController@delete')->name('delete');